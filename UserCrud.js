// ====================================================User Inputs
let inp_id=document.getElementById('inp_id')
let inp_user=document.getElementById('inp_user')
let inp_email=document.getElementById('inp_email')
let inp_pro_type=document.getElementById('inp_pro_type')
let inp_qty=document.getElementById("inp_qty")
let sumbitbtn=document.getElementById("sumbit")

let tbody=document.getElementById("tbody")
// ================================================Other Inputs
let tdy=new Date();


    let tdy_date=tdy.toUTCString();




function sumbit(){
    if(!inp_id.value || !inp_user.value || !inp_email.value || inp_pro_type.value=="none" || inp_qty .value=="0"){
        alert("Please Fill All The Inputs")
    }
    else{
        let customers=JSON.parse(localStorage.getItem("CustomerList")||("[]"))
        let dummy_register={
            id:inp_id.value,
            customername:inp_user.value,
            email:inp_email.value,
            type:inp_pro_type.value,
            qty:inp_qty.value,
            time:tdy_date
        }
        customers.push(dummy_register)
        localStorage.setItem("CustomerList",JSON.stringify(customers))

        setTimeout(()=>{
        inp_id.value=""
        inp_email.value=""
        inp_qty.value=""
        inp_user.value=""
        inp_pro_type.value=""
        },500)
        
    }
}

// ===============================================CREATE TABLE
function createTable(){
    let Customers=JSON.parse(localStorage.getItem("CustomerList"))
    if(Customers){
        Customers.map((user)=>{
            let newRow=tbody.insertRow(-1)
    
            let cell1=newRow.insertCell(0)
            let cell2=newRow.insertCell(1)
            let cell3=newRow.insertCell(2)
            let cell4=newRow.insertCell(3)
            let cell5=newRow.insertCell(4)
            let cell6=newRow.insertCell(5)
            let cell7=newRow.insertCell(6)
            let cell8=newRow.insertCell(7)
            let cell9=newRow.insertCell(8)
            
            cell1.innerText=user.id
            cell2.innerText=user.customername
            cell3.innerText=user.email
            cell4.innerText=user.type
            cell5.innerText=user.qty
            cell6.innerText=user.time
            cell7.innerHTML=`<button class="btn btn-primary btn-sm" onclick="editRow(this)">Edit</button>`
            cell8.innerHTML=`<button class="btn btn-danger btn-sm" onclick="deleteRow(this)">Delete</button>`
            cell9.innerHTML=`<button class="btn btn-success btn-sm" onclick="update(this)">Update</button>`

            document.getElementById('create').style.display="none"
            document.getElementById('delete').style.display="block"
        })
    }
    else{
        alert("Purchase History is Empty")
    }

    // ============================================================deleteTable
}
function deleteTable(){
        tbody.innerHTML=""
        document.getElementById('create').style.display="block"
        document.getElementById('delete').style.display="none"
}
// ========================================deleteRow()
function deleteRow(td){
    let json_list=JSON.parse(localStorage.getItem("CustomerList"))
    selectedRows=td.parentElement.parentElement
    let temp_id=selectedRows.cells[0].innerHTML

    setTimeout(()=>{
        for(i=0;i<json_list.length;i++){
            if(temp_id==json_list[i].id){
                json_list.splice(i,1)
            }
            localStorage.setItem("CustomerList",JSON.stringify(json_list))
            deleteTable();
            createTable();
        }
    },200)
}
// ========================================EditRow()
function editRow(td){
    selectedRows=td.parentElement.parentElement

    inp_id.value=selectedRows.cells[0].innerHTML
    inp_user.value=selectedRows.cells[1].innerHTML
    inp_email.value=selectedRows.cells[2].innerHTML
    inp_pro_type.value=selectedRows.cells[4].innerHTML
    inp_qty.value=selectedRows.cells[5].innerHTML
    sumbitbtn.style.display="none"
}
// =========================================UpdateRow()
function update(td){
    let json_list=JSON.parse(localStorage.getItem("CustomerList"))
    selectedRows=td.parentElement.parentElement

    let temp_id=selectedRows.cells[0].innerHTML

    setTimeout(()=>{
        for(i=0;i<json_list.length;i++){
            if(temp_id==json_list[i].id){
                json_list[i].id=inp_id.value;json_list[i].customername=inp_user.value;
                json_list[i].email=inp_email.value;json_list[i].type=inp_pro_type.value;json_list[i].qty=inp_qty.value;json_list[i].time=tdy_date;
            }
            localStorage.setItem("CustomerList",JSON.stringify(json_list));

            deleteTable();
            createTable();
        }
    },1000)
    sumbitbtn.style.display="block"
}